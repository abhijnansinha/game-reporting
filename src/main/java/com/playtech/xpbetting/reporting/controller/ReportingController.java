package com.playtech.xpbetting.reporting.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.playtech.xpbetting.reporting.model.GameEventData;
import com.playtech.xpbetting.reporting.service.FileService;
import com.playtech.xpbetting.reporting.service.MappingService;
import com.playtech.xpbetting.reporting.service.ReportingService;

/*Even though we are naming it as a Controller to have this as App Starting point.
This is not an actual Rest Controller hence Stereotype Annotation used is @Component*/

@Component
public class ReportingController {

	private static final String SESSION_WAGERED_WON_HEADER = "PLAYER_ID,SESSION_ID,AMOUNT_WAGERED,AMOUNT_WON";
	private static final String GAME_WAGERED_WON_HEADER = "PLAYER_ID,GAME_NAME,AMOUNT_WAGERED,AMOUNT_WON";
	private static final String SESSION_GAME_WAGERED_WON_HEADER = "PLAYER_ID,SESSION_ID,GAME_NAME,AMOUNT_WAGERED,AMOUNT_WON";
	private static final String TOP_FIVE_PROFITABLE_PLAYERS = "PLAYER_ID";

	@Value("${input.game.event.data}")
	private String inputGameEventFileName;

	@Value("${output.session.wagered.won}")
	private String outputSessionWageredWonFileName;

	@Value("${output.game.wagered.won}")
	private String outputGameWageredWonFileName;

	@Value("${output.session.game.wagered.won}")
	private String outputSessionGameWageredWonFileName;

	@Value("${output.top.five.profitable.players}")
	private String outputTopFiveProfitPlayersFileName;

	@Autowired
	private ReportingService reportingService;

	@Autowired
	private FileService fileService;

	@Autowired
	private MappingService mappingService;

	public List<GameEventData> getGameEvents() {

		return fileService.readFile(inputGameEventFileName).parallelStream().map(mappingService.mapToGameEventData)
				.collect(Collectors.toList());
	}

	public void createSessionWageredWon(List<GameEventData> inputData) {

		fileService.writeFile(outputSessionWageredWonFileName, SESSION_WAGERED_WON_HEADER,
				reportingService.generateSessionWageredWon(inputData));
	}

	public void createGameWageredWon(List<GameEventData> inputData) {
		fileService.writeFile(outputGameWageredWonFileName, GAME_WAGERED_WON_HEADER,
				reportingService.generateGameWageredWon(inputData));
	}

	public void createSessionGameWageredWon(List<GameEventData> inputData) {
		fileService.writeFile(outputSessionGameWageredWonFileName, SESSION_GAME_WAGERED_WON_HEADER,
				reportingService.generateSessionGameWageredWon(inputData));
	}

	public void createTopFiveProfitablePlayers(List<GameEventData> inputData) {
		fileService.writeFile(outputTopFiveProfitPlayersFileName, TOP_FIVE_PROFITABLE_PLAYERS,
				reportingService.generateTopFivePlayers(inputData));
	}

}
