package com.playtech.xpbetting.reporting.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.playtech.xpbetting.reporting.model.GameEventData;

@Service
public class ReportingService {

	private static final String BET = "Bet";
	private static final String WIN = "Win";
	private final DecimalFormat df = new DecimalFormat("###.##");

	public List<String> generateSessionWageredWon(List<GameEventData> gameEventList) {

		Map<String, Map<Long, Map<String, Double>>> masterMap = gameEventList.parallelStream()
				.filter(g -> (g.getChapter().contains(WIN) || g.getChapter().contains(BET))).collect(
						Collectors.groupingBy(GameEventData::getPlayerId,
								Collectors.groupingBy(GameEventData::getSessionId,
										Collectors.groupingBy(GameEventData::getChapter,
												Collectors.summingDouble(GameEventData::getAmount)))));

		List<String> outputList = new ArrayList<String>();
		StringBuilder line = new StringBuilder();

		for (Map.Entry<String, Map<Long, Map<String, Double>>> mainMap : masterMap.entrySet()) {
			String temp = mainMap.getKey();
			for (Map.Entry<Long, Map<String, Double>> innerMap : mainMap.getValue().entrySet()) {
				line.append(temp + ",");
				line.append(innerMap.getKey() + ",");
				line.append(
						innerMap.getValue().get(BET) == null ? "," : df.format(innerMap.getValue().get(BET)) + ",");
				line.append(innerMap.getValue().get(WIN) == null ? "" : df.format(innerMap.getValue().get(WIN)));
				outputList.add(line.toString());
				line.setLength(0);
			}
			temp = null;
		}

		return outputList;
	}

	public List<String> generateGameWageredWon(List<GameEventData> gameEventList) {

		Map<String, Map<String, Map<String, Double>>> masterMap = gameEventList.parallelStream()
				.filter(g -> (g.getChapter().contains(WIN) || g.getChapter().contains(BET))).collect(
						Collectors.groupingBy(GameEventData::getPlayerId,
								Collectors.groupingBy(GameEventData::getGameName,
										Collectors.groupingBy(GameEventData::getChapter,
												Collectors.summingDouble(GameEventData::getAmount)))));

		List<String> outputList = new ArrayList<String>();
		StringBuilder line = new StringBuilder();

		for (Map.Entry<String, Map<String, Map<String, Double>>> mainMap : masterMap.entrySet()) {
			String temp = mainMap.getKey();
			for (Map.Entry<String, Map<String, Double>> innerMap : mainMap.getValue().entrySet()) {
				line.append(temp + ",");
				line.append(innerMap.getKey() + ",");
				line.append(
						innerMap.getValue().get(BET) == null ? "," : df.format(innerMap.getValue().get(BET)) + ",");
				line.append(innerMap.getValue().get(WIN) == null ? "" : df.format(innerMap.getValue().get(WIN)));
				outputList.add(line.toString());
				line.setLength(0);
			}
			temp = null;
		}

		return outputList;
	}

	public List<String> generateSessionGameWageredWon(List<GameEventData> gameEventList) {

		Map<String, Map<Long, Map<String, Map<String, Double>>>> masterMap = gameEventList.parallelStream()
				.filter(g -> (g.getChapter().contains(WIN) || g.getChapter().contains(BET)))
				.collect(Collectors.groupingBy(GameEventData::getPlayerId,
						Collectors.groupingBy(GameEventData::getSessionId,
								Collectors.groupingBy(GameEventData::getGameName,
										Collectors.groupingBy(GameEventData::getChapter,
												Collectors.summingDouble(GameEventData::getAmount))))));

		List<String> outputList = new ArrayList<String>();
		StringBuilder line = new StringBuilder();

		for (Map.Entry<String, Map<Long, Map<String, Map<String, Double>>>> mainMap : masterMap.entrySet()) {
			String tempPlayer = mainMap.getKey();
			for (Map.Entry<Long, Map<String, Map<String, Double>>> middleMap : mainMap.getValue().entrySet()) {
				Long tempSession = middleMap.getKey();
				for (Map.Entry<String, Map<String, Double>> innerMap : middleMap.getValue().entrySet()) {
					line.append(tempPlayer + ",");
					line.append(tempSession + ",");
					line.append(innerMap.getKey() + ",");
					line.append(innerMap.getValue().get(BET) == null ? ","
							: df.format(innerMap.getValue().get(BET)) + ",");
					line.append(
							innerMap.getValue().get(WIN) == null ? "" : df.format(innerMap.getValue().get(WIN)));
					outputList.add(line.toString());
					line.setLength(0);
				}
				tempSession = null;
			}
			tempPlayer = null;
		}

		return outputList;
	}

	public List<String> generateTopFivePlayers(List<GameEventData> gameEventList) {

		Map<String, Double> masterMap = gameEventList.parallelStream().filter(g -> g.getChapter().contains(BET))
				.collect(Collectors.groupingBy(GameEventData::getPlayerId,
						Collectors.summingDouble(GameEventData::getAmount)));

		List<String> outputList = masterMap.entrySet().parallelStream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(5)

				.map(entry -> entry.getKey()).collect(Collectors.toList());

		return outputList;
	}
}
