package com.playtech.xpbetting.reporting.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

@Service
public class FileService {

	public List<String> readFile(String path) {

		List<String> dataList = new ArrayList<>();
		Path filePath = Paths.get(path);
		if (Files.exists(filePath)) {
			try (Stream<String> stream = Files.lines(filePath)) {
				dataList = stream.skip(1).collect(Collectors.toList());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return dataList;

	}

	public void writeFile(String path, String header, List<String> outputDataList) {

		Path filePath = Paths.get(path);
		File file = new File(path);
		try {
			if (file.exists())
				Files.delete(filePath);

			file.createNewFile();
			Files.write(filePath, header.getBytes(), StandardOpenOption.APPEND);

			for (String outputLine : outputDataList)
				Files.write(filePath, ("\n" + outputLine).getBytes(), StandardOpenOption.APPEND);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
