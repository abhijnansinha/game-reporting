package com.playtech.xpbetting.reporting.service;

import java.text.SimpleDateFormat;
import java.util.function.Function;

import org.springframework.stereotype.Service;

import com.playtech.xpbetting.reporting.model.GameEventData;

@Service
public class MappingService {

	public Function<String, GameEventData> mapToGameEventData = (line) -> {

		String[] data = line.split(",");

		GameEventData item = new GameEventData();

		item.setCode(data[0]);
		item.setPlayerId(data[1]);
		item.setAmount(Double.valueOf(data[2]));
		try {
			item.setEventDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data[3]));
		} catch (Exception e) {
		}
		item.setChapter(data[4]);
		item.setPartnerId(Integer.valueOf(data[5]));
		item.setGameName(data[6]);
		item.setSessionId(Long.valueOf(data[7]));

		return item;
	};

}
