package com.playtech.xpbetting.reporting;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.playtech.xpbetting.reporting.controller.ReportingController;
import com.playtech.xpbetting.reporting.model.GameEventData;

@SpringBootApplication
public class GameReportingApplication implements CommandLineRunner {

	@Autowired
	ReportingController controller;

	public static void main(String[] args) {
		SpringApplication.run(GameReportingApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		
		//Load Data from Input File
		List<GameEventData> inputData = controller.getGameEvents();
		
		//Generate Output Files
		controller.createSessionWageredWon(inputData);
		controller.createGameWageredWon(inputData);
		controller.createSessionGameWageredWon(inputData);
		controller.createTopFiveProfitablePlayers(inputData);
	}

}
